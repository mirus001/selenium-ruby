require "selenium-webdriver"
require 'test/unit'
include Test::Unit::Assertions

driver = 0
wait = 0
expected_window_count = 1

Before do
client = Selenium::WebDriver::Remote::Http::Default.new
client.timeout = 5 # seconds

driver = Selenium::WebDriver.for(:firefox, :http_client => client)
wait = Selenium::WebDriver::Wait.new(:timeout => 30)
end

google_url = "http://www.google.com"
  
Given(/^that an instance of Firefox is open$/) do

end

Given(/^navigate to "(.*?)"$/) do |url|
  driver.navigate.to ["http://", url].join
  
end

Given(/^search for "(.*?)"$/) do |search_string|
  
  element = driver.find_element(:name, 'q') # finding the input field
  element.send_keys search_string           # searching for "Eloqua"
  element.send_keys :return                 # sending return

end

Then(/^"(.*?)" should appear on the result page$/) do |arg1|
  # wait until the results page is shown before proceeding...
  # the container with id of "res" is contains the links found in the search
  wait.until { driver.find_element(:id, "search").displayed? }
  
  # retrieve the container that has the result elements
  result = driver.find_element(:id, "search")
  
  # find only results that contain Eloqua in the link text
  result.find_element(:link, arg1)
end

When(/^I click on the first result on the page$/) do
  
  # find only results that contain Eloqua in the link text
  results = driver.find_elements(:xpath, "//div[@id='search']//*[@href]")
  results[0].click
end

Then(/^"(.*?)" should appear in the url of the opened page$/) do |expected_url|
  # verify that the browser lands on the eloqua page
  begin
    wait.until {not (driver.current_url.include? google_url)}
  rescue
  end
  assert (driver.current_url.include? expected_url)  
end


Given(/^navigate to test file "(.*?)"$/) do |url|
  driver.navigate.to ["file:", Dir.pwd, "/test_files/", url].join
end

When(/^I click the "(.*?)" button$/) do |button_text|
  click_modal_button(driver, button_text)
  expected_window_count+=1
end

Then(/^a popup window appears$/) do
  actual_count = driver.window_handles.size
  assert(actual_count == expected_window_count, ["Expected ", expected_window_count, " but found ", actual_count].join)
end

When(/^I click the "(.*?)" button in the popup$/) do |button_text|
  click_modal_button(driver, button_text)
  expected_window_count+=1
end

When(/^I click the close button in all of the popup windows$/) do
  handles = driver.window_handles
  
  # close all modals
  for i in handles
    driver.switch_to.window(i)
    
    if(driver.title == "Child Window")
      click_modal_button(driver, "Close")
    end
  end
end

Then(/^only the initial window is visible$/) do
  handles = driver.window_handles
  assert(handles.size == 1, "More than one window found")
  driver.switch_to.window(driver.window_handles.last)
  assert(driver.title == "Parent Window", "Parent window not found")
end

After do
  driver.quit
end

def click_modal_button(driver, button_text)
  css_search_value = ["input[type=\"button\"][value=\"", button_text, "\"]"].join

  # wait until the button to show the modal is pressent
  wait = Selenium::WebDriver::Wait.new(:timeout => 10)
  wait.until{driver.find_element(:css, css_search_value).displayed?}
  element_to_click = driver.find_element(:css, css_search_value)
  
  begin 
    element_to_click.click
  rescue
    # WebDriver will hang on this call (known issue with modals) so wrapping this in a block
    #   to catch the exception thrown will allow this test to continue
    #   # the exception is net/protocol.rb:146:in `rescue in rbuf_fill': Timeout::Error (Timeout::Error)
  end
end


