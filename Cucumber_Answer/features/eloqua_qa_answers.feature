Feature: Eloqua QA Anwers
    This feature is a collection of scenarios that are related to Eloqua QA interview
    answers.
 
 	@question_1
 	Scenario: Question 1 - Open and close multiple modal popups
 		Given that an instance of Firefox is open
 		And navigate to test file "parent.htm"
 		When I click the "Open Modal Popup" button
 		Then a popup window appears
 		When I click the "Open Modal Popup" button in the popup
 		Then a popup window appears
 		When I click the close button in all of the popup windows
 		Then only the initial window is visible
 	
 	@question_2	 
 	Scenario: Question 2 - Search for a term in google on Firefox
 		Given that an instance of Firefox is open
 		And navigate to "www.google.com"
 		And search for "Eloqua"
 		Then "Eloqua" should appear on the result page
 		When I click on the first result on the page
 		Then "www.eloqua.com" should appear in the url of the opened page
 		
 		