# Eloqua QA Position Test Question 2
# Author: Mirus Lu
# Email: mirus001@gmail.com

# This script is a Ruby script that uses Selenium Webdriver to do the following
# 1) Opens an instance of firefox
# 2) Navigates to Google.com
# 3) Searches for "Eloqua" and submits the query
# 4) Verifies that "Eloqua" is shown in the result page
# 5) Clicks on the top result and verifies that the Eloqua page is loaded

require "selenium-webdriver"

search_page = "http://google.com"
search_string = "Eloqua"
expected_result_page = "www.eloqua.com"

driver = Selenium::WebDriver.for :firefox
wait = Selenium::WebDriver::Wait.new(:timeout => 30)

driver.navigate.to search_page 

element = driver.find_element(:name, 'q') # finding the input field
element.send_keys search_string           # searching for "Eloqua"
element.send_keys :return                 # sending return

# wait until the results page is shown before proceeding...
# the container with id of "res" is contains the links found in the search
wait.until { driver.find_element(:id, "res").displayed? }

# retrieve the container that has the result elements
results = driver.find_elements(:xpath, "//div[@id='search']//*[@href]")

# find only results that contain Eloqua in the link text
results[0].click

# verify that the browser lands on the eloqua page
wait.until {driver.current_url.include? expected_result_page}

# cleanup
driver.quit