# Eloqua QA Position Test Question 1
# Author: Mirus Lu
# Email: mirus001@gmail.com

# This script is a Ruby script that uses Selenium Webdriver to do the following
# 1) Write a program using Selenium Webdriver
# 2) Open up the "parent.htm" file in Firefox.
# 3) Then click the "Open Modal Popup" button.
# 4) In the pop-up, click "Open Modal Popup" again.
# 5) Close all windows by clicking the Close buttons in the Pop-Ups
# 6) Use any programming languages and libraries you need

# Notes:
# There is a known issue where WebDriver hangs waiting for an HTTP response when a modal is shown
# A timeout exception is thrown when the WebDriver hangs for more than a given timeout


require "selenium-webdriver"

test_page = ["file:", Dir.pwd, "/test_files/parent.htm"].join
open_button = "Open Modal Popup"
close_button = "Close"

def click_modal_button(driver, button_text)
  css_search_value = ["input[type=\"button\"][value=\"", button_text, "\"]"].join

  # wait until the button to show the modal is pressent
  wait = Selenium::WebDriver::Wait.new(:timeout => 10)
  wait.until{driver.find_element(:css, css_search_value).displayed?}
  element_to_click = driver.find_element(:css, css_search_value)
  
  begin 
    element_to_click.click
  rescue
    # WebDriver will hang on this call (known issue with modals) so wrapping this in a block
    #   to catch the exception thrown will allow this test to continue
    #   # the exception is net/protocol.rb:146:in `rescue in rbuf_fill': Timeout::Error (Timeout::Error)
  end
end

# By forcing the client to have a timeout to 5 seconds we reduce the hang time to 5 seconds when
# a modal is shown
client = Selenium::WebDriver::Remote::Http::Default.new
client.timeout = 5 # seconds

driver = Selenium::WebDriver.for(:firefox, :http_client => client)
  
# go to the test page
driver.navigate.to test_page

click_modal_button(driver, open_button)
print "after first click, window count is ", driver.window_handles.size
#window count should be greater now

window_count = driver.window_handles.size
click_modal_button(driver, open_button)
print "after second click, window count is ", driver.window_handles.size
#window count should be greater again

# retrieve the window handles to be used when closing modals
handles = driver.window_handles

# close all modals
for i in handles
  driver.switch_to.window(i)
  print ["\n", driver.title].join
  
  if(driver.title == "Child Window")
    print "\nclicking close button"
    click_modal_button(driver, "Close")
  end
end

# cleanup
driver.quit

